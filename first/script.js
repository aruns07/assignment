(function() {
    'use strict';
    var dataUrl = 'https://jsonplaceholder.typicode.com/posts',
        serverData = [],
        $userForm,
        $userId, $userTitle, $userBody;

    function fetchUserData() {
        // When fetch is not supported we can user XMLHTTPRequest
        // To avoid usage of jQuery I used latest API.
        // To support older browsers I would use jQuery's Ajax method with promise.
        return fetch(dataUrl).then(function(response) {
                    return response.json();
                }).then(function(response) {
                    return response;
                });
    }

    function fillUserIdOptions() {
        var filteredUsers = serverData.map(function(user) {
                                return parseInt(user.userId, 10);
                            }).sort(function(a, b) {
                                return a - b;
                            });

        var usedId = '',
            option;
        filteredUsers.forEach(function(currentId) {
            if (usedId != currentId) {
                usedId = currentId
                option = document.createElement('option');
                option.value = option.text = currentId;
                $userId.appendChild(option);
            }
        });
    }

    function fillUserTitleOptions(userId) {
        var option,
            filteredUsers = serverData.filter(function(data) {
                                return data.userId === parseInt(userId, 10);
                            });

        $userTitle.innerHTML = '';
        filteredUsers.forEach(function(user) {
            option = document.createElement('option');
            option.value = user.body;
            option.text = user.title;
            $userTitle.appendChild(option);
        });
    }


    function userIdSelected() {
        var selectedUserId,
            userIdType = '',
            selectedIndex = $userId.selectedIndex;

        $userForm.classList.remove('selected-odd');
        $userForm.classList.remove('selected-even');

        if (selectedIndex > 0) {
            selectedUserId = $userId.options[selectedIndex].value;
            userIdType = (selectedUserId % 2 === 0) ? 'even' : 'odd';
            $userForm.classList.add('selected-' + userIdType);

            fillUserTitleOptions(selectedUserId);
            userTitleSelected();
        }
    }

    function userTitleSelected() {
        $userBody.value = $userTitle.selectedOptions[0].value;
    }

    function bindEvents() {
        $userId.addEventListener('change', userIdSelected);
        $userTitle.addEventListener('change', userTitleSelected);
    }

    function init() {
        $userForm = document.querySelector('.userForm');
        $userId = $userForm.querySelector('.userId');
        $userTitle = $userForm.querySelector('.userTitle');
        $userBody = $userForm.querySelector('.userBody');

        bindEvents();

        fetchUserData().then(function(data) {
            serverData = data;
            fillUserIdOptions();
        });
    }

    init();
})();
