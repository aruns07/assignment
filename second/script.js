var namespace = {};

/*
    ObjectBox having feature of creating object on request
 */
namespace.ObjectBox = (function() {
    'use strict';
    function ObjectBox(id, ground) {
        this.$objectBox = $('#' + id);
        this.ground = ground;
        this.init();
        this.bindEvents();
    }

    ObjectBox.prototype.createSquare = function() {
        var rec = new fabric.Rect({ top: 100, left: 100, width: 50, height: 50, fill: '#f55' });
        this.sendObjectToGround(rec);
    };

    ObjectBox.prototype.createTriangle = function() {
        var triangle = new fabric.Triangle({ top: 100, left: 100, width: 50, height: 50, fill: 'blue' });
        this.sendObjectToGround(triangle);
    };

    ObjectBox.prototype.createCircle = function() {
        var circle = new fabric.Circle({ top: 100, left: 100, radius: 25, fill: 'green' });
        this.sendObjectToGround(circle);
    };

    ObjectBox.prototype.sendObjectToGround = function(object) {
        object.hasControls = false;
        object.hasBorders = false;

        this.ground.addObject(object);
        this.$objectBox.trigger('object-created', [{object: object}]);
    };

    ObjectBox.prototype.bindEvents = function() {
        this.$objectBox.find('.create-object.object-square').on('click', this.createSquare.bind(this));
        this.$objectBox.find('.create-object.object-triangle').on('click', this.createTriangle.bind(this));
        this.$objectBox.find('.create-object.object-circle').on('click', this.createCircle.bind(this));
    };

    ObjectBox.prototype.init = function() {
        var actionButtonString = '<button class=\'create-object object-square\'>Square</button>';
        actionButtonString += '<button class=\'create-object object-triangle\'>Triangle</button>';
        actionButtonString += '<button class=\'create-object object-circle\'>Circle</button>';

        this.$objectBox.html(actionButtonString);
    };

    return ObjectBox;
})();

/*
    Ground name space having feature for one canvas
 */
namespace.Ground = (function() {
    'use strict';
    function Ground(id) {
        this.$canvas = $('#' + id);
        this.canvas = new fabric.Canvas(id, {selection:false});
        this.edges = {
            startY: this.$canvas.offset().top,
            endY: this.$canvas.offset().top + this.$canvas.height(),
            startX: this.$canvas.offset().left,
            endX: this.$canvas.offset().left + this.$canvas.width(),
        };
    }

    Ground.prototype.addObject = function(object) {
        this.canvas.add(object);
    };

    return Ground;

})();

/*
    module to bootstrap the application
 */
(function($, namespace) {
    'use strict';
    var currentObject,
        inObjectEventX, inObjectEventY,
        $container = $('.container'),
        groundsName = ['one', 'two'],
        grounds = [],
        objectBoxes = [];

    function listenBoxEvents(e, data) {
        data.object.on('mousedown', function(data) {
            currentObject = this;
            inObjectEventX = data.e.layerX - currentObject.getLeft();
            inObjectEventY = data.e.layerY - currentObject.getTop();
            $container.on('mousemove', trackObjectMove);
        });

        data.object.on('mouseup', function() {
            currentObject = undefined;
            $container.off('mousemove', trackObjectMove);
        });
    }

    function trackObjectMove(event) {
        var eventY = event.pageY,
            eventX = event.pageX;

        grounds.filter(function(ground) {
            //the ground/canvas within wich pointer is present.
            return (currentObject &&
                    eventY > ground.edges.startY && eventY < ground.edges.endY &&
                    eventX > ground.edges.startX && eventX < ground.edges.endX);
        }).forEach(function(ground) {

            currentObject.remove();
            ground.canvas.add(currentObject);

            window.requestAnimationFrame(function() {
                currentObject.setTop(eventY - ground.edges.startY - inObjectEventY);
                currentObject.setLeft(eventX - ground.edges.startX - inObjectEventX);
            });
        });
    }

    function init() {
        grounds = groundsName.map(function(name) {
                        return new namespace.Ground('ground-' + name);
                    });

        objectBoxes = groundsName.map(function(name, index) {
                        return new namespace.ObjectBox('object-box-' + name, grounds[index]);
                    });

        objectBoxes.map(function(box) {
            box.$objectBox.on('object-created', listenBoxEvents);
        });
    }

    init();

})($, namespace);
